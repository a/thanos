import discord
from discord.ext import commands
from discord.ext.commands import Cog


class Thanos(Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.is_owner()
    @commands.command(aliases=["snap"])
    async def purge(self, ctx, limit: int, channel: discord.TextChannel = None):
        """Clears a given number of messages, owner only."""
        if not channel:
            channel = ctx.channel
        limit_remaining = limit
        while limit_remaining > 100:
            await channel.purge(limit=100)
            limit_remaining -= 100

        await channel.purge(limit=limit_remaining)


def setup(bot):
    bot.add_cog(Thanos(bot))
